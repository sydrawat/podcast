const express = require("express");
const podcastController = require("./controllers/podcastController");

const app = express();
global.directory = __dirname;

//setting up template engine
app.set("view engine", "ejs");

//Invoke controllers
podcastController(app);

//static files
app.use(express.static("./public"));
app.use(express.static("./node_modules"));
app.use(express.static("./images"));

//listeners for the port
app.listen(80);
console.log("you are listening to port 80");
